import os

from flask import Flask, url_for
app = Flask(__name__)

@app.route('/')
def index():
    # return "Index Page"
    # reference the function and use url_for; then OK to change urls
    return url_for('show_user_profile', username='richard')

@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile
    return "User %s" % username

@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show post
    return "Post %s" % post_id

@app.route('/hello')
def hello_world():
    # import pdb; pdb.set_trace()
    # i = 3
    # i = i + 1
    # visited = i
    # return "You've visited " + str(visited) + " times"
    return "Hello World!"
    
if __name__ == '__main__':
    host = os.getenv('IP', '0.0.0.0')
    port = int(os.getenv('PORT', 5000))
    app.debug = True
    app.run(host=host, port=port)
